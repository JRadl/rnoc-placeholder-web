import React from "react";
import HeroSection from "./../components/HeroSection";
import FeaturesSection from "./../components/FeaturesSection";
import Footer from "../components/Footer";
import './index.scss';

class IndexPage extends React.Component{
  constructor(){
    super();
    this.state = {
      dark: false,
    }
  }
  render(){
    return (
      <div className={this.state.dark?"dark":""}>
        <HeroSection
          color="white"
          size="medium"
          backgroundImage=""
          backgroundImageOpacity={1}
          title={<span onClick={() => this.setState({dark:!this.state.dark})}>R&ndash;NOC</span>}
          subtitle={<div>R&ndash;NOC je skupina přednáškových NOCí, založená pro&nbsp;jednodušší spolupráci a&nbsp;předávání znalostí mezi&nbsp;organizátory. Na tomto webu v blízké době poběží nový registrační systém na přednáškové noci.</div>}
          image="img/mapa-NOCi.svg"
        ></HeroSection>
        <FeaturesSection
          color="white"
          size="medium"
          backgroundImage=""
          backgroundImageOpacity={1}
          title="Přednáškové NOCi"
          subtitle={<span>Přednášková NOC je akcí studentů studentům, kde se v&nbsp;noci v&nbsp;prostorách gymnázií a středních škol přednáší vážně i&nbsp;nevážně.</span>}
          darkTheme={this.state.dark}
        ></FeaturesSection>
        <Footer
          color="white"
          size="normal"
          backgroundImage=""
          backgroundImageOpacity={1}
          copyright="© 2019 Company"
          logo="https://uploads.divjoy.com/logo.svg"
        ></Footer>
      </div>
    );
  }
}

export default IndexPage;
