import React from "react";
import Section from "./Section";

function Footer(props) {
  return (
    <Section
      color={props.color}
      size={props.size}
      backgroundImage={props.backgroundImage}
      backgroundImageOpacity={props.backgroundImageOpacity}
      style={{textAlign:"center"}}
    >
      © Pravoúhlý sněm 2020 <br></br>
      jakub.radl@brnoc.cz
    </Section>
  );
}

export default Footer;
