import React from "react";
import Section from "./Section";
import SectionHeader from "./SectionHeader";
import Features from "./Features";

function FeaturesSection(props) {
  return (
    <Section
      color={props.color}
      size={props.size}
      backgroundImage={props.backgroundImage}
      backgroundImageOpacity={props.backgroundImageOpacity}
    >
      <div className="container">
        <SectionHeader
          title={props.title}
          subtitle={props.subtitle}
          size={3}
          spaced={true}
          className="has-text-centered"
        ></SectionHeader>
        <Features
          items={[
            {
              title: "PraNOC (PNOC, KNOC)",
              description:
                <span>Stará a&nbsp;tradiční přednášková NOC. Sídlí v&nbsp;Praze. Probíhá dvakrát ročně (na&nbsp;jaře jako KNOC, na&nbsp;podzim jako PNOC).</span>,
              image: "img/PNOC.png",
              year: "2016",
              link: "http://pnoc.cz"
            },
            {
              title: "BrNOC",
              description:
                <span>Největší z&nbsp;přednáškových NOCí. Sídlí v&nbsp;Brně. Většinou probíhá jednou ročně v&nbsp;prosinci.</span>,
              image: "img/BrNOC.png",
              year: "2016",
              link: "https://brnoc.cz"
            },
            {
              title: "HraNoL",
              description:
                <span>Menší přednášková dvojNOC. Sídlí v&nbsp;Hradci Králové. Probíhá jednou ročně v&nbsp;únoru.</span>,
              image: "img/HraNoL.svg",
              year: "2017",
              link: "https://hranol.gybon.cz"
            },
            {
              title: "PLNOC",
              description:
                <span>Mladá přednášková NOC. Sídlí v&nbsp;Plzni. Probíhá jednou ročně na&nbsp;podzim.</span>,
              image: "img/PLNOC.png",
              year: "2019",
              link: "https://plnoc.cz"
            },
            {
              title: "Π noc",
              description:
                <span>Mladá přednášková NOC. Sídlí v&nbsp;Ostravě. Probíhá jednou ročně.</span>,
              image: "img/PiNOC.svg",
              year: "2019",
              link: "https://pinoc.cz"
            },
            {
              title: "BudeNOC",
              description:
                <span>Mladá přednášková NOC. Sídlí v&nbsp;Českých Budějovicích. Bude první ročník.</span>,
              image: props.darkTheme?"img/BudeNOC_dark.png":"img/BudeNOC_light.png",
              year: "2020",
              link: "https://spssecb.cz/budenoc"
            }
          ]}
        ></Features>
      </div>
    </Section>
  );
}

export default FeaturesSection;
