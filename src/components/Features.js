import React from "react";
import "./Features.scss";
import { Link } from "../util/router";

function Features(props) {
  return (
    <div className="Features">
      {props.items.map((item, index) => (
        
          <a
            className="Features__columns columns is-variable is-8 is-vcentered has-text-centered-mobile"
            key={index}
            href={item.link}
            target="_blank"
          >
            <div className="column is-half">
              <h3 className="Features__title title has-text-weight-bold is-spaced is-3">
                {item.title}
              </h3>
              <p className="subtitle">{item.description}<br />Since {item.year}<br />{item.link}</p>
            </div>
            <div className="column">
              <figure className="Features__image image">
                <img src={item.image} alt={item.title} class={item.title}></img>
              </figure>
            </div>
          </a>
        
      ))}
    </div>
  );
}

export default Features;
